#startstat
say Привет, милые котята!♥

#say 1
schedule clear stats:fish 
schedule function stats:fish 30s append
#say 2
schedule clear stats:level 
schedule function stats:level 60s append
schedule clear stats:kills 
schedule function stats:kills 90s append
schedule clear stats:leave 
schedule function stats:leave 120s append
schedule clear stats:live 
schedule function stats:live 150s append
schedule clear stats:crouch 
schedule function stats:crouch 180s append
schedule clear stats:walk 
schedule function stats:walk 210s append
schedule clear stats:sprint 
schedule function stats:sprint 240s append
schedule clear stats:swim 
schedule function stats:swim 270s append
schedule clear stats:fall 
schedule function stats:fall 300s append
schedule clear stats:horse 
schedule function stats:horse 330s append
schedule clear stats:fly 
schedule function stats:fly 360s append
schedule clear stats:jump 
schedule function stats:jump 390s append
schedule clear stats:damage_deal 
schedule function stats:damage_deal 420s append
schedule clear stats:damage_take 
schedule function stats:damage_take 450s append
schedule clear stats:enchant 
schedule function stats:enchant 480s append
schedule clear stats:feed 
schedule function stats:feed 510s append
schedule clear stats:trade 
schedule function stats:trade 540s append
schedule clear stats:trap 
schedule function stats:trap 570s append
schedule clear stats:flower 
schedule function stats:flower 600s append
schedule clear stats:music 
schedule function stats:music 630s append
schedule clear stats:sleep 
schedule function stats:sleep 660s append
schedule clear stats:timerest 
schedule function stats:timerest 690s append
schedule clear stats:stone 
schedule function stats:stone 720s append
schedule clear stats:coal 
schedule function stats:coal 750s append
schedule clear stats:diamond 
schedule function stats:diamond 780s append
schedule clear stats:emerald 
schedule function stats:emerald 810s append
schedule clear stats:gold 
schedule function stats:gold 840s append
schedule clear stats:iron 
schedule function stats:iron 870s append
schedule clear stats:lapis 
schedule function stats:lapis 900s append
schedule clear stats:quartz 
schedule function stats:quartz 930s append
schedule clear stats:redstone 
schedule function stats:redstone 960s append

schedule clear stats:startstats 
schedule function stats:startstats 990s append


