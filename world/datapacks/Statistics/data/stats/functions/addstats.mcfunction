#addstats
say addstats

scoreboard objectives add myhealth health "Здоровье"
scoreboard objectives add mylevel level "Уровень"
scoreboard objectives add mykills totalKillCount "Убил"

scoreboard objectives add myleave custom:leave_game "Ливал"
scoreboard objectives add mylive custom:time_since_death "Жил"
scoreboard objectives add mycrouch_one_cm custom:crouch_one_cm "Крался"
scoreboard objectives add mywalk_one_cm custom:walk_one_cm "Прошел"
scoreboard objectives add mysprint_one_cm custom:sprint_one_cm "Пробежал"
scoreboard objectives add myswim_one_cm custom:swim_one_cm "Плыл"
scoreboard objectives add myfall_one_cm custom:fall_one_cm "Падал"
scoreboard objectives add myhorse_one_cm custom:horse_one_cm "Верхом"
scoreboard objectives add myaviate_one_cm custom:aviate_one_cm "Пролетел"
scoreboard objectives add myjump custom:jump "Подпрыгнул"
scoreboard objectives add mydamage_dealt custom:damage_dealt "Киборг-убийца"
scoreboard objectives add mydamage_taken custom:damage_taken "Огрёб"
scoreboard objectives add myenchant_item custom:enchant_item "Зачаровал"
scoreboard objectives add myanimals_bred custom:animals_bred "Размножил"
scoreboard objectives add myfish_caught custom:fish_caught "Рыбачил"
scoreboard objectives add mytrader custom:traded_with_villager "Торговал"
scoreboard objectives add mytrapped custom:trigger_trapped_chest "Попадался"
scoreboard objectives add mypot_flower custom:pot_flower "Наводил красоту"
scoreboard objectives add myplay_record custom:play_record "Ставил Музыку"
scoreboard objectives add mysleep_in_bed custom:sleep_in_bed "Спал"
scoreboard objectives add mysince_rest custom:time_since_rest "Не Спал"
scoreboard objectives add mystone mined:stone "Камнелом"
scoreboard objectives add mycoal mined:coal_ore "Угля добыто"
scoreboard objectives add mydiamond mined:diamond_ore "Алмазов добыто"
scoreboard objectives add myemerald mined:emerald_ore "Изумрудов добыто"
scoreboard objectives add mygold mined:gold_ore "Золота добыто"
scoreboard objectives add myiron mined:iron_ore "Железа добыто"
scoreboard objectives add mylapis mined:lapis_ore "Лазурита добыто"
scoreboard objectives add myquartz mined:nether_quartz_ore "Кварца добыто"
scoreboard objectives add myredstone mined:redstone_ore "Краснокамня добыто"
#scoreboard objectives add ""

scoreboard objectives setdisplay list myhealth
scoreboard objectives setdisplay belowname myhealth
scoreboard objectives modify myhealth displayname [{"text":"/ 20 "},{"text":"♥","color":"red"}]

